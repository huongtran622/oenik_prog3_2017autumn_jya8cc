﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace CatchingEggs
{

    /// <summary>
    /// The class for controling game playing
    /// </summary>
    class GameLogic
    {
        static Random r = new Random();

        public GameModel gm;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// Constructor takes 1 parameter from GameModel class 
        /// </summary>
        /// <param name="newgm">GameModel</param>
        public GameLogic(GameModel newgm)
        {
            gm = newgm;
        }

        /// <summary>
        /// The are 2 types of direction of Basket is Left and Right
        /// </summary>
        public enum BasketDirection { Left, Right }

        /// <summary>
        /// This method for moving the Basket to the Left or the Right
        /// </summary>
        /// <param name="dir">Direction of the Basket with enum type</param>
        public void BasketMove(BasketDirection dir)
        {
            int diff1 = -10;
            int diff2 = 10;

            // If the direction is to the Left then the Basket move faster according to how many times you pressed the left key
            if (dir == BasketDirection.Left)
            {
                diff1--;
                gm.basket.ChangeX(diff1);
            }

            // If the direction is to the Right then the Basket move faster according to how many times you pressed the right key
            else
            {
                diff2++;
                gm.basket.ChangeX(diff2);
            }
        }

        /// <summary>
        ///  This method to Move the eggs
        /// </summary>
        /// <param name="w">the width of the screen</param>
        /// <param name="h">the height of the screen</param>
        /// <param name="time">The time during the game playing</param>
        public void EggsMove(double w, double h, int time)
        {
            // 4 eggs falling down with randomly speed
            for (int i = 1; i < gm.eggs.Count; i++)
            {
                if (gm.eggs[i].Egg.Center.Y <= 0)
                {
                    gm.eggs[i].ChangeY(r.Next(4 + i, 10 + i));
                }

                // When the eggs are on the screen and time is less than 25 seconds
                else if (gm.eggs[i].Egg.Center.Y > 0 && gm.eggs[i].Egg.Center.Y < h - 50 && time < 25)
                {
                    gm.eggs[i].ChangeY(2 * i + 4);
                }

                // When the eggs are still on the screen but the time is greater than 25 seconds, it will change its speed, much faster
                else if (gm.eggs[i].Egg.Center.Y > 0 && gm.eggs[i].Egg.Center.Y < h - 50 && time >= 25)
                {
                    gm.eggs[i].ChangeY(7 + 2 * i);
                }

                // When it is out of the screen
                else
                {
                    gm.eggs[i].ChangeY(1);
                }
            }

            // Violet falling down like ziczac shape
            if (gm.eggs[0].Egg.Center.Y < 105)
            {
                gm.eggs[0].ChangeY(10);
            }

            if (gm.eggs[0].Egg.Center.Y >= 105 && gm.eggs[0].Egg.Center.Y < 200)
            {
                gm.eggs[0].ChangeX(10);
                gm.eggs[0].ChangeY(10);
            }

            if (gm.eggs[0].Egg.Center.Y >= 200 && gm.eggs[0].Egg.Center.Y < 300)
            {
                gm.eggs[0].ChangeX(-20);
                gm.eggs[0].ChangeY(10);
            }

            if (gm.eggs[0].Egg.Center.Y >= 300 && gm.eggs[0].Egg.Center.Y < 400)
            {
                gm.eggs[0].ChangeX(20);
                gm.eggs[0].ChangeY(10);
            }

            if (gm.eggs[0].Egg.Center.Y >= 400 && gm.eggs[0].Egg.Center.Y < 500)
            {
                gm.eggs[0].ChangeX(-20);
                gm.eggs[0].ChangeY(10);
            }

            if (gm.eggs[0].Egg.Center.Y >= 500)
            {
                gm.eggs[0].ChangeY(5);
            }

            // Check if the eggs hit the basket the score increased and random that egg to other positions above Y = 0
            for (int i = 1; i < 5; i++)
            {
                if (gm.eggs[i].Egg.Center.Y < h - 70 &&
                   gm.basket.Basket.FillContainsWithDetail(gm.eggs[i].Egg).Equals(IntersectionDetail.Intersects))
                {
                    gm.Score++;
                    gm.eggs[i].ChangeXY(gm.Positions[r.Next(0, 7)], r.Next(-500, -50));
                }

                // If the eggs fall out of the screen
                if (gm.eggs[i].Egg.Center.Y > h - 25)
                {
                    gm.eggs[i].ChangeXY(gm.Positions[r.Next(0, 7)], r.Next(-500, -50));
                }
            }

            // If the egg VIOLET fall out of the screen 
            if (gm.eggs[0].Egg.Center.Y > h - 25)
            {
                gm.eggs[0].ChangeXY(gm.Positions[r.Next(0, 7)], r.Next(-500, -50));
            }
        }
    }
}
