﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatchingEggs
{
    public class GameModel
    {
        public int[] Positions { get; set; }

        static Random rnd = new Random();

        public int Score { get; set; }

        public OneBasket basket { get; set; }

        public List<OneEgg> eggs { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// Create the objects of the game
        /// </summary>
        /// <param name="w">the width of the screen</param>
        /// <param name="h">the hight of the screen</param>
        public GameModel(double w, double h)
        {
            // There are 8 position of chicken decorations . Eggs will fall from it
            Positions = new int[8] { 150, 250, 350, 450, 550, 650, 750, 850 };

            basket = new OneBasket(w / 2, h - 60);
            eggs = new List<OneEgg>();

            // Create 5 types of eggs with randomly position above Y = 0 (y<0)
            for (int i = 0; i < 5; i++)
            {
                eggs.Add(new OneEgg(Positions[rnd.Next(0, 7)], rnd.Next(-(int)h, -50)));
            }
        }
    }
}
