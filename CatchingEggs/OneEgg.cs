﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace CatchingEggs
{
    public class OneEgg
    {
        public EllipseGeometry Egg { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="OneEgg"/> class.
        /// This is Constructor that take 2 parameters
        /// </summary>
        /// <param name="newx">The new X coordinate of the egg</param>
        /// <param name="newy">the new Y coordinate of the egg</param>
        public OneEgg(double newx, double newy)
        {
            Egg = new EllipseGeometry(new Rect(newx, newy, 30, 40));
        }

        // Set the new position for the egg
        public void ChangeXY(double newx, double newy)
        {
            Egg.Center = new Point(newx, newy);
        }

        // Changing Y coordinate of the egg
        public void ChangeY(double diff)
        {
            Egg.Center = new Point(Egg.Center.X, Egg.Center.Y + diff);
        }

        // Changing X coordinate of the egg
        public void ChangeX(double diff)
        {
            Egg.Center = new Point(Egg.Center.X + diff, Egg.Center.Y);
        }
    }
}
