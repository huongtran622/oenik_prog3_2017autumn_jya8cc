﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CatchingEggs
{
    /// <summary>
    /// Interaction logic for CheckCancel.xaml
    /// </summary>
    public partial class CheckCancel : Window
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="CheckCancel"/> class.
        /// Create CheckCancel window take no parameters
        /// </summary>
        public CheckCancel()
        {
            InitializeComponent();

            ImageBrush checkbg = new ImageBrush(new BitmapImage(new Uri("Resources/checkbg.jpg", UriKind.Relative)));
            mygrid.Background = checkbg;
        }

        /// <summary>
        /// Check if user click to button Yes
        /// </summary>
        /// <param name="sender">click event</param>
        /// <param name="e">button</param>
        private void Yes_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        /// <summary>
        /// Check if user clicks to button NO
        /// </summary>
        /// <param name="sender">Click event</param>
        /// <param name="e">button</param>
        private void No_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }
    }
}
