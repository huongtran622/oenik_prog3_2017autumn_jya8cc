﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CatchingEggs
{
    /// <summary>
    /// Interaction logic for Finish.xaml
    /// </summary>
    public partial class Finish : Window
    {
        public Viewmodel vm { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Finish"/> class.
        /// Finish window created
        /// </summary>
        /// <param name="score">The score of the user after game finished</param>
        public Finish(string score)
        {
            InitializeComponent();
            vm = new Viewmodel();
            vm.Score = score;
            DataContext = vm;
            ImageBrush bg = new ImageBrush(
                                new BitmapImage(new Uri("Resources/finishbg.jpg", UriKind.Relative)));
            Gr.Background = bg;
        }

        /// <summary>
        /// Check if user click button
        /// </summary>
        /// <param name="sender">user click</param>
        /// <param name="e">button response</param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
