﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Media;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace CatchingEggs
{

    /// <summary>
    /// GameScreen from FrameworkElement
    /// </summary>
    class GameControl : FrameworkElement
    {
        GameModel GM;
        GameLogic Logic;
        Stopwatch stw;
        double w;
        double h;
        DispatcherTimer timer;
        Typeface font = new Typeface("Cooper Black");
        Point textLoc = new Point(10, 10);
        Rect exit;
        List<Rect> explosion = new List<Rect>();
        SoundPlayer sndbackground;
        SoundPlayer bombExplosion;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameControl"/> class.
        /// Constructor
        /// </summary>
        public GameControl()
        {
            Loaded += GameControl_Loaded;
        }

        private void GameControl_Loaded(object sender, RoutedEventArgs e)
        {
            w = ActualWidth;
            h = ActualHeight;
            exit = new Rect(900, 400, 200, 200);
            GM = new GameModel(w, h);
            Logic = new GameLogic(GM);

            Window win = Window.GetWindow(this);
            if (win != null)
            {
                // Background music sound
                sndbackground = new SoundPlayer(CatchingEggs.Resource1.Backgroundsound);
                sndbackground.PlayLooping();

                // Sound of the explosion
                bombExplosion = new SoundPlayer(CatchingEggs.Resource1.Bombsound);
                bombExplosion.Load();

                // Background picture
                ImageBrush ib = new ImageBrush(new BitmapImage(new Uri("Resources/house.jpg", UriKind.Relative)));
                ib.TileMode = TileMode.None;
                ib.Viewport = new Rect(0, 0, w, h);
                ib.ViewportUnits = BrushMappingMode.Absolute;
                win.Background = ib;

                stw = new Stopwatch();
                stw.Start();

                win.PreviewKeyDown += Win_PreviewKeyDown;
                win.MouseMove += Win_MouseMove;
                win.MouseDown += Win_MouseDown;
                timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromMilliseconds(40);
                timer.Tick += Timer_Tick;
                timer.Start();
            }

            InvalidateVisual();
        }

        /// <summary>
        /// This method for capturing mouse click
        /// </summary>
        /// <param name="sender">mouse click</param>
        /// <param name="e">mouse captured</param>
        private void Win_MouseDown(object sender, MouseButtonEventArgs e)
        {
            // If user click on Exit, then Exit window opens
            if (exit.Contains(e.GetPosition(this)))
            {
                timer.Stop();
                CheckCancel checkwin = new CheckCancel();

                // Checkwindow appeared in the center of MainWindow
                checkwin.WindowStartupLocation = WindowStartupLocation.CenterScreen;

                // If user click OK in the Checkwindow then the check window and MainWindow will be closed
                if (checkwin.ShowDialog() == true)
                {
                    checkwin.Close();
                    Window.GetWindow(this).Close();
                }
                else
                {
                    // otherwise the game will continue
                    checkwin.Close();
                    timer.Start();
                }
            }

            InvalidateVisual();
        }

        // Mouse Moving on the screen
        private void Win_MouseMove(object sender, MouseEventArgs e)
        {
            Point p = e.GetPosition(this);
            InvalidateVisual();

            // If the exit pictures contains the mouse position, the change it to another bigger rect
            if (exit.Contains(p))
            {
                exit = new Rect(900, 400, 250, 250);
            }
            else
            {
                exit = new Rect(900, 400, 200, 200);
            }

            InvalidateVisual();
        }

        // Capture the key pressed
        private void Win_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Left: Logic.BasketMove(GameLogic.BasketDirection.Left); break;
                case Key.Right: Logic.BasketMove(GameLogic.BasketDirection.Right); break;
            }
        }

        /// <summary>
        /// This is method for eggs moving constantly and catch the time ending
        /// </summary>
        /// <param name="sender">The timer who started the event.</param>
        /// <param name="e">Args of the event.</param>
        private void Timer_Tick(object sender, EventArgs e)
        {
            int time = stw.Elapsed.Seconds;
            Logic.EggsMove(w, h, time);

            // If the basket catch the Violet Egg. The game will be over
            if (GM.eggs[0].Egg.Center.Y < h - 70 && GM.basket.Basket.FillContainsWithDetail(GM.eggs[0].Egg).Equals(IntersectionDetail.Intersects))
            {
                sndbackground.Stop();
                bombExplosion.Play();
                timer.Stop();
                stw.Stop();
                explosion.Add(new Rect(w / 2 - 150, h / 2 - 200, 400, 400));
                explosion.Add(new Rect(200, 200, 250, 250));
                explosion.Add(new Rect(400, 400, 300, 300));
                explosion.Add(new Rect(700, 300, 400, 400));
                explosion.Add(new Rect(500, 100, 350, 350));
            }

            // After 45 seconds of game, the new window will be opened, game is finished
            if (stw.Elapsed.Seconds == 45)
            {
                stw.Stop();
                timer.Stop();
                sndbackground.Stop();
                Finish win2 = new Finish(GM.Score.ToString());
                win2.WindowStartupLocation = WindowStartupLocation.CenterScreen;

                // Save scores and Name
                if (win2.ShowDialog() == true)
                {
                    StreamWriter sw = File.AppendText("Resources/Result.txt");
                    if (win2.vm.Name != null)
                    {
                        sw.WriteLine(String.Format("{0},{1} ", win2.vm.Name, GM.Score.ToString()));
                    }
                    else
                    {
                        sw.WriteLine(String.Format("Unknown,{0} ", GM.Score.ToString()));
                    }

                    sw.Close();
                }
            }

            InvalidateVisual();
        }

        /// <summary>
        /// Drawing everything on the screen
        /// </summary>
        /// <param name="drawingContext">drawing</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            if (GM != null)
            {
                // Drawing 8 chickens above of the screen
                ImageBrush chicken = new ImageBrush(new BitmapImage(new Uri("Resources/chicken.png", UriKind.Relative)));
                drawingContext.DrawRectangle(chicken, null, new Rect(100, 3, 100, 100));
                drawingContext.DrawRectangle(chicken, null, new Rect(200, 3, 100, 100));
                drawingContext.DrawRectangle(chicken, null, new Rect(300, 3, 100, 100));
                drawingContext.DrawRectangle(chicken, null, new Rect(400, 3, 100, 100));
                drawingContext.DrawRectangle(chicken, null, new Rect(500, 3, 100, 100));
                drawingContext.DrawRectangle(chicken, null, new Rect(600, 3, 100, 100));
                drawingContext.DrawRectangle(chicken, null, new Rect(700, 3, 100, 100));
                drawingContext.DrawRectangle(chicken, null, new Rect(800, 3, 100, 100));

                // Just for decorate
                ImageBrush decorate = new ImageBrush(new BitmapImage(new Uri("Resources/decorate.png", UriKind.Relative)));
                drawingContext.DrawRectangle(decorate, null, new Rect(1070, 520, 130, 150));

                // 5 types of eggs
                ImageBrush violet = new ImageBrush(new BitmapImage(new Uri("Resources/violet.png", UriKind.Relative)));
                ImageBrush turquoise = new ImageBrush(new BitmapImage(new Uri("Resources/turquoise.png", UriKind.Relative)));
                ImageBrush pink = new ImageBrush(new BitmapImage(new Uri("Resources/pink.png", UriKind.Relative)));
                ImageBrush green = new ImageBrush(new BitmapImage(new Uri("Resources/green.png", UriKind.Relative)));
                ImageBrush blue = new ImageBrush(new BitmapImage(new Uri("Resources/blue.png", UriKind.Relative)));
                ImageBrush brokenegg = new ImageBrush(new BitmapImage(new Uri("Resources/broken.png", UriKind.Relative)));
                List<ImageBrush> images = new List<ImageBrush>();
                images.Add(violet); // Egg 0
                images.Add(turquoise); // Egg 1
                images.Add(pink); // Egg 2
                images.Add(green); // Egg 3
                images.Add(blue); // Egg 4

                // Drawing eggs
                for (int i = 0; i < GM.eggs.Count; i++)
                {
                    if (GM.eggs[i].Egg.Center.Y > 105 && GM.eggs[i].Egg.Center.Y < h - 50)
                    {
                        drawingContext.DrawEllipse(images[i], null, GM.eggs[i].Egg.Center, GM.eggs[i].Egg.RadiusX, GM.eggs[i].Egg.RadiusY);
                    }
                    else if (GM.eggs[i].Egg.Center.Y >= h - 50)
                    {
                        drawingContext.DrawEllipse(brokenegg, null, GM.eggs[i].Egg.Center, GM.eggs[i].Egg.RadiusX, GM.eggs[i].Egg.RadiusY);
                    }
                }

                // Drawing Basket
                ImageBrush basketimage = new ImageBrush(new BitmapImage(new Uri("Resources/basket.png", UriKind.Relative)));
                drawingContext.DrawRectangle(basketimage, null, GM.basket.Basket.Rect);

                // Drawing Score and Time on the screen
                FormattedText score = new FormattedText("Score: ",
                                                        CultureInfo.CurrentCulture,
                                                        FlowDirection.LeftToRight,
                                                        new Typeface("Comic Sans MS"),
                                                        30,
                                                        Brushes.Black);
                score.SetFontWeight(FontWeights.Bold, 0, 5);
                drawingContext.DrawText(score, new Point(10, 10));

                FormattedText text = new FormattedText(GM.Score.ToString(),
                                                       CultureInfo.CurrentCulture,
                                                       FlowDirection.LeftToRight,
                                                       new Typeface("Comic Sans MS"),
                                                       50,
                                                       Brushes.Black);
                drawingContext.DrawText(text, new Point(20, 50));

                FormattedText timetxt = new FormattedText("Time: ",
                                                       CultureInfo.CurrentCulture,
                                                       FlowDirection.LeftToRight,
                                                       new Typeface("Comic Sans MS"),
                                                       30,
                                                       Brushes.Black);
                timetxt.SetFontWeight(FontWeights.Bold, 0, 4);
                drawingContext.DrawText(timetxt, new Point(10, 110));

                FormattedText time = new FormattedText(stw.Elapsed.Seconds.ToString(),
                                                       CultureInfo.CurrentCulture,
                                                       FlowDirection.LeftToRight,
                                                       new Typeface("Comic Sans MS"),
                                                       50,
                                                       Brushes.Black);
                drawingContext.DrawText(time, new Point(20, 140));

                // Drawing Exit sign on the screen
                ImageBrush exitsign = new ImageBrush(new BitmapImage(new Uri("Resources/exit.png", UriKind.Relative)));
                drawingContext.DrawRectangle(exitsign, null, exit);

                // Just for decorate
                ImageBrush decorate1 = new ImageBrush(new BitmapImage(new Uri("Resources/decorate1.png", UriKind.Relative)));
                drawingContext.DrawRectangle(decorate1, null, new Rect(950, 10, 200, 150));

                // Drawing explosion pictures and game over picture
                ImageBrush bum = new ImageBrush(new BitmapImage(new Uri("Resources/bum.gif", UriKind.Relative)));
                ImageBrush gameover = new ImageBrush(new BitmapImage(new Uri("Resources/gameover.png", UriKind.Relative)));
                if (explosion.Count != 0)
                {
                    for (int i = 1; i < explosion.Count; i++)
                    {
                        drawingContext.DrawRectangle(bum, null, explosion[i]);
                    }

                    drawingContext.DrawRectangle(gameover, null, explosion[0]);
                }

                // boardshow = new Rect(950, 180, 200, 200);
                ImageBrush board = new ImageBrush(new BitmapImage(new Uri("Resources/wood.jpg", UriKind.Relative)));
                drawingContext.DrawRectangle(board, new Pen(Brushes.Black, 2), new Rect(950, 180, 200, 200));

                // Read from Result file
                StreamReader sr = new StreamReader("Resources/Result.txt");

                List<string[]> list = new List<string[]>();
                while (!sr.EndOfStream)
                {
                    string[] temp = sr.ReadLine().Split(',');
                    list.Add(temp);
                }

                // Order the score from highest to lowest score
                if (list.Count >= 2)
                {
                    list = list.OrderByDescending(arr => arr[1]).ToList();
                }

                // Creating new list to put 8 strings which is for 8 highest score
                List<string> texts = new List<string>();
                for (int i = 0; i < list.Count; i++)
                {
                    if (i < 8)
                    {
                        string[] ex = list[i];
                        string exToString = ex[0] + "- Score: " + ex[1];
                        texts.Add(exToString);
                    }
                }

                sr.Close();

                // TOP 8 Users have the highest score
                if (texts.Count >= 1)
                {
                    for (int i = 0; i < texts.Count; i++)
                    {
                        FormattedText tx1 = new FormattedText(texts[i],
                                                           CultureInfo.CurrentCulture,
                                                           FlowDirection.LeftToRight,
                                                           font,
                                                           15,
                                                           Brushes.Black);
                        tx1.SetForegroundBrush(
                            new LinearGradientBrush(
                            Colors.White,
                            Colors.OliveDrab,
                            90.0),
                            0,
                            texts[i].Count());
                        tx1.SetFontWeight(FontWeights.Bold, 0, texts[i].Count());
                        drawingContext.DrawText(tx1, new Point(983, 185 + i * 26 ));
                    }
                }
            }
        }
    }
}
