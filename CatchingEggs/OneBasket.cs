﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace CatchingEggs
{
    public class OneBasket
    {
        public RectangleGeometry Basket { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="OneBasket"/> class.
        /// Constructor to create a new basket
        /// </summary>
        /// <param name="x">the x coordinate of the basket</param>
        /// <param name="y">the y coordinate of the basket</param>
        public OneBasket(double x, double y)
        {
            Basket = new RectangleGeometry(new Rect(x, y, 80, 60));
        }

        // This method for changing X coordinate of the Basket
        public void ChangeX(double diff)
        {
            Basket.Rect = new Rect(Basket.Rect.X + diff, Basket.Rect.Y, 80, 60);
        }
    }
}
